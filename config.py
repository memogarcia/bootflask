# -*- coding: utf-8 -*-

import os
basedir = os.path.abspath(os.path.dirname(__file__))


# admin list
ADMINS = [os.environ.get('ADMIN_EMAIL_LIST')]


# OpenID integration
OPENID_PROVIDERS = [
    {'name': 'Google', 'url': 'https://www.google.com/accounts/o8/id'},
    {'name': 'Yahoo', 'url': 'https://me.yahoo.com'}
]


# database configuration
# this configuration is taken from Flask Megatutorial
# http://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')