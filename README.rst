bootflask: A bootstrap flask project
====================================

bootflask is an Apache2 Licensed blueprint for flask applications.

Installation
------------

    $ git clone https://github.com/memogarcia/bootflask.git


Features
--------
- Separation between views, models, forms, error handling, logging and decorators
- Separation of run and run_debug
- Database creation, migrate, downgrade, upgrade


Database configuration
----------------------

    # create a new database in SQLite

    $ python db_create.py

    # migrate to a new version of the database

    $ python db_migrate.py
    
